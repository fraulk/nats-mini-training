# NATS

## Introduction

La **messaging queue**, ou **file d'attente de message** est une technique de programmation utilisée pour la communication entre processus ou entre serveurs.  
Cela permet la liaison asynchrone entre des services/serveurs, c'est-à-dire qu'au lieu d'envoyer un message et attendre une réponse en bloquant les autres tâches, l'expéditeur envoi le message et passe à autre chose.  

La donnée est encodée en un **message** et est envoyée par un publicateur et reçu par un ou plusieurs abonnés.

![Schéma](schema/schema1.bmp)

## Intérêt

Cela permet une communication facilitée entre différents environnements, language service cloud.  
Les clients se connectent au serveur nats via une adresse URL et peuvent pub/sub.
NATS offre un "```at most once```" service. Si un abonné n'écoute pas un sujet ou est inactif, le message n'est pas reçu.

NATS permet la publication et l'écoute de message. Ces deux concepts dépendent grandement d'un "**Subject**" qui permettent de trier les messages en différents sujets. Un sujet n'est juste qu'une chaîne de caractère *alpha-numérique*
qui permet la liaison entre un publicateur et un abonné.

![schema](schema/schema5.bmp)

Le caractère ```.``` permet l'hiérarchisation des sujets, example :

```txt
time.us
time.us.east
time.us.east.atlanta
time.eu.east
time.eu.warsaw
```

![schema](schema/schema2.bmp)

## Wildcards

NATS permet l'écoute de plusieurs sujets grâce à des "Wildcards" via un seul abonnement (contrairement au publicateurs qui ne peuvent publier sur un sujet)  

### Example

Le caractère ```*``` va écouter n'importe quel sujet du même niveau, donc ```time.*.east``` écoutera ```time.us.east``` et ```time.eu.east```

![schema](schema/schema3.bmp)

Le caractère ```>``` va écouter les sous-sujets, donc ```time.us.>``` écoutera ```time.us.east``` et ```time.us.east.atlanta``` alors que ```time.us.*``` n'écoutera que ```time.us.east```

![schema](schema/schema4.bmp)

## Request-Reply

Une requête est envoyée et soit le programme attend une réponse avec un timeout soit reçois une réponse de manière asynchrone. La requête est publiée sur un sujet avec un sujet de réponse en retour, et les répondeurs écoute le sujet et envoie une réponse au sujet de réponse.

![schema](schema/schema6.bmp)

## Queue Groups

Les *queues* permettent la tolérance d'erreurs. Lorsqu'un message est publié dans une *queue*, un abonnés aléatoire reçoit le message

![schema](schema/schema7.bmp)

## Acknowledgements

Parfois les messages peuvent se perdre. Si le programme fait une request/reply, il devrait utiliser des timeouts pour gérer les erreurs réseau ou logiciel.  
C'est une bonne pratique que de mettre souvent des timeouts sur des requêtes et de gérer les timeouts.  
Lorsque un message est publié, il est toujours bon de vérifier la réception correct du message en faisant une reply, d'où le concept d'acknowledgements, ou ACKs.  
Un ACK peut être tout simplement un message vide, ce qui évite une grande utilisation de la bande passante.

![schema](schema/schema8.bmp)

## Sequence Numbers

A problème récurrent pour les messages One-To-Many est qu'ils peuvent être perdus à cause d'une erreur réseau. Un manière simple de corriger ceci est d'inclure un id de sequence avec le message. Les abonnées pourront vérifier si ils ont raté un message de cette manière.

![schema](schema/schema9.bmp)

Garder en mémoire que :

- Chaque publicateur doit utiliser sa propre sequence
- Si possible, les abonnées pouvoir demander les messages manquant via l'id

Vous pouvez envoyer les ids dans les message ou dans les sujets. Par example, un publicateur peut envoyer un message dans le sujet ```updates.1```, ```updates.2```, etc... et l'abonné peut écouter le sujet ```updates.*``` et parser le sujet pour avoir l'id

---

[La doc complète](https://docs.nats.io/nats-concepts/intro)
