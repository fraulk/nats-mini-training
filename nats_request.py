import asyncio
import json
from nats.aio.client import Client as NATS
from nats.aio.utils import new_inbox

async def example():
    nc = NATS()

    await nc.connect(servers=["nats://localhost:4222"])

    # Send the request
    try:
        msg = await nc.request("time", b'temps ?', timeout=10)
        print("Requested time")
        # Use the response
        print("Reply : ", msg.data)
        print("time is " + str(msg.data))
    except asyncio.TimeoutError:
        print("Timed out waiting for response")

    await nc.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()
