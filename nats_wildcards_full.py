import asyncio
from nats.aio.client import Client as NATS

async def example():
    nc = NATS()
    await nc.connect(servers=["nats://localhost:4222"])

    # Use queue to wait for messages to arrive
    queue = asyncio.Queue()
    async def cb(msg):
        await queue.put_nowait(msg)

    await nc.subscribe("time.*.east", cb=cb)
    await nc.subscribe("time.>", cb=cb)

    await nc.publish("time.us.east", b'from * or >')
    await nc.publish("time.us.east.atlanta", b'from >')

    await nc.publish("time.eu.east", b'from * or >')
    await nc.publish("time.eu.east.warsaw", b'from >')

    msg_A = await queue.get()
    msg_B = await queue.get()
    msg_C = await queue.get()
    msg_D = await queue.get()
    msg_E = await queue.get()
    msg_F = await queue.get()
    
    print("The first two came from * wildcard")
    print("Subject A : " + str(msg_A.subject) + ", Message : " + str(msg_A.data))
    print("Subject C : " + str(msg_C.subject) + ", Message : " + str(msg_C.data))
    print("The others came from > wildcards")
    print("Subject E : " + str(msg_E.subject) + ", Message : " + str(msg_E.data))
    print("Subject B : " + str(msg_B.subject) + ", Message : " + str(msg_B.data))
    print("Subject F : " + str(msg_F.subject) + ", Message : " + str(msg_F.data))
    print("Subject D : " + str(msg_D.subject) + ", Message : " + str(msg_D.data))

    await nc.close()

    # [end wildcard_tester]

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()