import asyncio
from nats.aio.client import Client as NATS

async def example():
    nc = NATS()

    await nc.connect(servers=["nats://localhost:4222"])

    future = asyncio.Future()

    async def cb(msg):
        nonlocal future
        future.set_result(msg)

    await nc.subscribe("updates", queue="workers", cb=cb)
    await nc.publish("updates", b'All is Well')

    msg = await asyncio.wait_for(future, 3)
    print("Subject : " + str(msg.subject) + ", Data : " + str(msg.data))

loop = asyncio.get_event_loop()
loop.run_until_complete(example())