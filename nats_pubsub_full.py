import asyncio
from nats.aio.client import Client as NATS

async def example():
  nc = NATS()
  await nc.connect(servers=["nats://localhost:4222"])
  # https://docs.nats.io/developing-with-nats/receiving/async
  future = asyncio.Future()
  
  # Message handler: la fonction qui sera executé à chaque message reçu
  async def cb(msg):
    nonlocal future
    future.set_result(msg)
  
  await nc.subscribe("updates", cb=cb)
  await nc.publish("updates", b'All is Well')
  await nc.flush()
  
  # Wait for message to come in
  msg = await asyncio.wait_for(future, 1)
  
  print("Subject : " + str(msg.subject) + ", Data : " + str(msg.data))
  
  await nc.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()