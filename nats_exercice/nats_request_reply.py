import asyncio
import json
from nats.aio.client import Client as NATS
from nats.aio.utils import new_inbox

async def example():
    nc = NATS()

    async def sub(msg):
        await nc.publish(msg.reply, b'13:37')

    await nc.connect(servers=["nats://localhost:4222"])
    await nc.subscribe("time", cb=sub)

    # https://docs.nats.io/developing-with-nats/sending/request_reply

    await nc.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()
