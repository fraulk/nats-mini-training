import asyncio
from nats.aio.client import Client as NATS

async def example():
    nc = NATS()
    await nc.connect(servers=["nats://localhost:4222"])

    # https://docs.nats.io/developing-with-nats/receiving/wildcards

    # Use queue to wait for messages to arrive
    queue = asyncio.Queue()
    async def cb(msg):
        await queue.put_nowait(msg)



    await nc.close()

    # [end wildcard_tester]

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()