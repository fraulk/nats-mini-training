import asyncio
from nats.aio.client import Client as NATS

async def example():
    nc = NATS()

    await nc.connect(servers=["nats://localhost:4222"])

    future = asyncio.Future()

    async def cb(msg):
        nonlocal future
        future.set_result(msg)

    # https://docs.nats.io/developing-with-nats/receiving/queues

loop = asyncio.get_event_loop()
loop.run_until_complete(example())