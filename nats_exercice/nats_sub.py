import asyncio # asyncio-nats-client
from nats.aio.client import Client as NATS

async def example():
    nc = NATS()
    await nc.connect(servers=["nats://localhost:4222"])
    # https://docs.nats.io/developing-with-nats/receiving/async
    # https://gitlab.com/fraulk/nats-mini-training

    async def cb(msg):
        print(msg.data)
    
    await nc.subscribe("updates", cb=cb)
    # msg = await asyncio.wait_for(future, 10)
    await asyncio.sleep(15)

    # print(msg.data)
    await nc.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()