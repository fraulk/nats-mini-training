import asyncio # asyncio-nats-client
from nats.aio.client import Client as NATS
    # https://docs.nats.io/developing-with-nats/receiving/async

async def example():
    nc = NATS()
    await nc.connect(servers=["nats://localhost:4222"])
    await nc.publish("updates", b'all is well')
    await nc.close()

loop = asyncio.get_event_loop()
loop.run_until_complete(example())
loop.close()